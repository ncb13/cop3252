/**
 * Created by Nikhil Bosshardt on 2/19/2017.
 */

public class KnightDriver {
    // main method begins execution of Java application
    public static void main(String[] args) {

        InputBox run = new InputBox();

        Knight player1 = run.getKnight();
        Stars star1 = run.getStars();
        run.outPut(player1, star1);


    } // end method main
} // end class Welcome1