/**
 * Created by Nikhil Bosshardt on 2/19/2017.
 */
public class Knight {

    String name = " ";
    int health = 0;
    int num_battles = 0;
    int age = 0;
    int gold = 0;

    Knight(String nm, int h, int n, int a, int g) {
        name = nm;
        health = h;
        num_battles = n;
        age = a;
        gold = g;
    }

    void simpleDraw(){
        System.out.printf("=-=-=-= Here is your hero =-=-=-=" + "\n");
        System.out.printf("Name:         " + name + "\n");
        System.out.printf("Health: %9d \n", health);
        System.out.printf("Battles:  %6d \n", num_battles);
        System.out.printf("Age:  %11d \n", age);
        System.out.printf("Gold:  %10d \n", gold);
        System.out.printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" + "\n");
    }

    String Draw(){
        String out = "============================\n";
        out = out + "Name: ";
        out = out + name;
        out = out + "\n";
        out = out + "Health: ";
        out = out + health;
        out = out + "\n";
        out = out + "Number of battles: ";
        out = out + num_battles;
        out = out + "\n";
        out = out + "Age: ";
        out = out + age;
        out = out + "\n";
        out = out + "Gold: ";
        out = out + gold;
        out = out + "\n";
        out = out + "============================";

        return out;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getNum_battles() {
        return num_battles;
    }

    public void setNum_battles(int num_battles) {
        this.num_battles = num_battles;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
