/**
 * Created by Nikhil Bosshardt on 2/19/2017.
 */
public class Stars {


    int row = 0;
    int col = 0;

    Stars (int r, int c){
        row = r;
        col = c;
    }
    String Draw(){

        String out = "\n";
        int rowCount = 0;
        int colCount = 0;

        while (rowCount != row) {

            while (colCount != col){
                out = out + "* ";
                colCount++;
            }
            colCount = 0;
            out = out + "\n";
            if ((rowCount%2) == 0){
                out = out + " ";
            }
            rowCount++;
        }
        return out;
    }


}
