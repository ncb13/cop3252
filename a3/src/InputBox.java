/**
 * Created by Nikhil Bosshardt on 2/19/2017.
 */

import javax.swing.*;
import java.awt.*;

public class InputBox {

    Knight getKnight() {


        JTextField nameBox = new JTextField(10);
        JTextField numBatBox = new JTextField(7);
        JTextField healthBox = new JTextField(10);
        JTextField ageBox = new JTextField(12);
        JTextField goldBox = new JTextField(10);

        JPanel myPanel = new JPanel();
        myPanel.setPreferredSize(new Dimension(200,170));
        myPanel.add(new JLabel("Name: "));
        myPanel.add(nameBox);
        myPanel.add(new JLabel("Health: "));
        myPanel.add(healthBox);
        myPanel.add(new JLabel("Number of Battles: "));
        myPanel.add(numBatBox);
        myPanel.add(new JLabel("Age: "));
        myPanel.add(ageBox);
        myPanel.add(new JLabel("Gold: "));
        myPanel.add(goldBox);


        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Enter his stats!", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println("Knight building...");
        }
        Knight knight1 = new Knight(
                nameBox.getText(),
                Integer.parseInt(healthBox.getText()),
                Integer.parseInt(numBatBox.getText()),
                Integer.parseInt(ageBox.getText()),
                Integer.parseInt(goldBox.getText())
        );
        return knight1;
    }

    Stars getStars(){
        JTextField rowBox = new JTextField(5);
        JTextField colBox = new JTextField(5);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Rows: "));
        myPanel.add(rowBox);
        myPanel.add(new JLabel("Columns: "));
        myPanel.add(colBox);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "You control the stars!", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println("Stars shifting...");
        }

        Stars star1 = new Stars(
                Integer.parseInt(rowBox.getText()),
                Integer.parseInt(colBox.getText()));

        return star1;
    }

    void outPut(Knight k1, Stars s1){
        JPanel myPanel = new JPanel();
        myPanel.add(new JTextArea(k1.Draw() + s1.Draw()));

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Your humble creation!", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            System.out.println("World ending...");
        }

    }

}